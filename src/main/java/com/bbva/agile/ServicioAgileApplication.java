package com.bbva.agile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioAgileApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioAgileApplication.class, args);
	}

}
