package com.bbva.agile;

import com.bbva.agile.examen.Examen;
import com.bbva.agile.examen.ResultadoFinal;
import com.bbva.agile.repositorios.ExamenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path= "/examen")
public class ExamenController {

    @Autowired
    ExamenDao examenDao;

    @GetMapping(path="/")
    public Examen getExamen(){
        return examenDao.getExamen();
    }

    @PostMapping(path="/resultados", consumes = "application/json", produces = "application/json")
    public ResultadoFinal getResultados(@RequestBody Examen examen){
       return examenDao.getResultado(examen);
    }

}
