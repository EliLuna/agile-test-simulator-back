package com.bbva.agile.repositorios;

import com.bbva.agile.examen.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ExamenDao {

    private static Examen examen= new Examen();
    private static Logger logger= LoggerFactory.getLogger(ExamenDao.class);


    static{


        String pregunta1="¿Quien es el responsable de la coordinación y supervisión de la ejecución del programa, de los proyectos bajo su cargo ?";
        ArrayList<String> respuestas1= new ArrayList<>();
        respuestas1.add("A. Stakeholders");
        respuestas1.add("B. Product Owner");
        respuestas1.add("C. Program Manager");
        Pregunta pregunta_1=new Pregunta(1, pregunta1, respuestas1);

        String pregunta2="Líder que ayuda a que el equipo entienda y madure prácticas y valores de agilidad";
        ArrayList<String> respuestas2= new ArrayList<>();
        respuestas2.add("A. Scrum Master");
        respuestas2.add("B. Product Owner");
        respuestas2.add("C. Program Manager");
        Pregunta pregunta_2=new Pregunta(2, pregunta2, respuestas2);

        String pregunta3="En una Inception, ¿el equipo agile qué puntos trata y cuál es su objetivo?";
        ArrayList<String> respuestas3= new ArrayList<>();
        respuestas3.add("A. Se toman las features e historias de usuario que se trabajarán en el Sprint.");
        respuestas3.add("B. Lo que se hizo ayer, lo que se hará hoy y los impedimentos que se detecten.");
        respuestas3.add("C. Capturar la misión y definir los usuarios, clientes, la Big Picture, el Backlog, Dependencias, el MVP y Release Planning.");
        Pregunta pregunta_3=new Pregunta(3, pregunta3, respuestas3);

        String pregunta4="¿Quién de ellos no forma parte como tal del equipo Scrum?";
        ArrayList<String> respuestas4= new ArrayList<>();
        respuestas4.add("A. Stakeholders");
        respuestas4.add("B. Product Owner");
        respuestas4.add("C. Program Manager");
        Pregunta pregunta_4=new Pregunta(4, pregunta4, respuestas4);

        String pregunta5="Cuales son las Sesiones recomendadas en base a Scrum:";
        ArrayList<String> respuestas5= new ArrayList<>();
        respuestas5.add("A. Daily,Planing,Rewiew,Refinamiento,Retro");
        respuestas5.add("B. Refinamiento,Planing,Retro,Daily,Análisis.");
        respuestas5.add("C. Diseño,Dayli,Plannig,Retro,Rewiew.");
        Pregunta pregunta_5=new Pregunta(5, pregunta5, respuestas5);

        String pregunta6="Cada cuantos recomendado realizar los sprints:";
        ArrayList<String> respuestas6= new ArrayList<>();
        respuestas6.add("A. 2 semanas");
        respuestas6.add("B. 3 semanas");
        respuestas6.add("C. 8 semanas");
        Pregunta pregunta_6=new Pregunta(6, pregunta6, respuestas6);

        ArrayList<Pregunta>listPreguntas= new ArrayList<>();
        listPreguntas.add(pregunta_1);
        listPreguntas.add(pregunta_2);
        listPreguntas.add(pregunta_3);
        listPreguntas.add(pregunta_4);
        listPreguntas.add(pregunta_5);
        listPreguntas.add(pregunta_6);

        examen.setIdExamen(1);
        examen.setNombreExamen("Examen1");
        examen.setPreguntas(listPreguntas);

    }

    public static ResultadoFinal getResultado(Examen examenResuelto){

        int contCorrecta=0;
        int contIncorrecta=0;

        ArrayList<Resultado> listResultado= new ArrayList<>();

        for(Pregunta pregunta: examenResuelto.getPreguntas()){
            Resultado resultado= new Resultado();
            int idPregunta=pregunta.getIdPregunta();
            String respCorrecta= RespuestaCorrecta.getRespuestas().get(idPregunta);
            Pregunta preguntaPorId=examen.getPreguntas().get(idPregunta-1);
            logger.debug("Pregunta :"+idPregunta +" Respuesta correcta :"+respCorrecta );
                if(pregunta.getRespuestas().get(0).equals(respCorrecta)){
                    contCorrecta++;
                    logger.debug("Respuestas correctas   :"+contCorrecta);
                    resultado.setEstatus("¡CORRECTO!");

                }else{
                    contIncorrecta++;
                    logger.debug("Respuestas incorrectas :"+contIncorrecta);
                    resultado.setEstatus("INCORRECTO");
                }
                resultado.setRespuestaSeleccionada(pregunta.getRespuestas().get(0));
                resultado.setRespuestaCorrecta(respCorrecta);
                resultado.setPregunta(preguntaPorId.getPregunta());

                listResultado.add(resultado);
        }

        String resultadoFinalEstatus="";
        if(contCorrecta>3){
            resultadoFinalEstatus="APROBADO";
        }else{
            resultadoFinalEstatus="VUELVE A INTENTARLO";
        }

        ResultadoFinal resultadoFinal= new ResultadoFinal(listResultado, resultadoFinalEstatus, contCorrecta, contIncorrecta);
        return  resultadoFinal;
    }

    public static void validaRespuestas(Examen examen){
        logger.debug(examen.toString());
    }

    public static Examen getExamen(){
        return examen;
    }

}
