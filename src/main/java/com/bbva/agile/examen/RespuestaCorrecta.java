package com.bbva.agile.examen;

import java.util.HashMap;
import java.util.Map;

public class RespuestaCorrecta {

    private static final String RESPUESTA1="C. Program Manager";
    private static final String RESPUESTA2="A. Scrum Master";
    private static final String RESPUESTA3="C. Capturar la misión y definir los usuarios, clientes, la Big Picture, el Backlog, Dependencias, el MVP y Release Planning.";
    private static final String RESPUESTA4="A. Stakeholders";
    private static final String RESPUESTA5="A. Daily,Planing,Rewiew,Refinamiento,Retro";
    private static final String RESPUESTA6="A. 2 semanas";

    public static Map<Integer,String> getRespuestas(){
        Map<Integer,String> mapRespuestas= new HashMap<>();
        mapRespuestas.put(1,RESPUESTA1);
        mapRespuestas.put(2,RESPUESTA2);
        mapRespuestas.put(3,RESPUESTA3);
        mapRespuestas.put(4,RESPUESTA4);
        mapRespuestas.put(5,RESPUESTA5);
        mapRespuestas.put(6,RESPUESTA6);
        return mapRespuestas;
    }


}
