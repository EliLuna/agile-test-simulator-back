package com.bbva.agile.examen;

import java.util.ArrayList;
import java.util.List;

public class Examen {

    private int idExamen=0;
    private String nombreExamen="";
    private ArrayList<Pregunta> preguntas;

    public Examen() {
    }

    public Examen(int idExamen, String nombreExamen, ArrayList<Pregunta> preguntas) {
        this.idExamen = idExamen;
        this.nombreExamen = nombreExamen;
        this.preguntas = preguntas;
    }

    public int getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(int idExamen) {
        this.idExamen = idExamen;
    }

    public String getNombreExamen() {
        return nombreExamen;
    }

    public void setNombreExamen(String nombreExamen) {
        this.nombreExamen = nombreExamen;
    }

    public ArrayList<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(ArrayList<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }
}
