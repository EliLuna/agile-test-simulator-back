package com.bbva.agile.examen;

import java.util.ArrayList;
import java.util.Map;

public class Pregunta {

    private int idPregunta=0;
    private String pregunta="";
    private ArrayList<String> respuestas;

    public Pregunta() {
    }

    public Pregunta(int idPregunta, String pregunta, ArrayList<String> respuestas) {
        this.idPregunta = idPregunta;
        this.pregunta = pregunta;
        this.respuestas = respuestas;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public ArrayList<String> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(ArrayList<String> respuestas) {
        this.respuestas = respuestas;
    }

    @Override
    public String toString() {
        return "Preguntas{" +
                "idPregunta=" + idPregunta +
                ", pregunta='" + pregunta + '\'' +
                ", respuestas=" + respuestas +
                '}';
    }
}
