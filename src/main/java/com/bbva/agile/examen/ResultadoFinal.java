package com.bbva.agile.examen;

import java.util.ArrayList;

public class ResultadoFinal {

    private String resultadoFinal="";
    private int respuestasCorrectas=0;
    private int respuestasIncorrectas=0;
    private ArrayList<Resultado> listaresultados;

    public ResultadoFinal() {
    }

    public ResultadoFinal(ArrayList<Resultado> listaresultados, String resultadoFinal, int respuestasCorrectas, int respuestasIncorrectas) {
        this.listaresultados = listaresultados;
        this.resultadoFinal = resultadoFinal;
        this.respuestasCorrectas = respuestasCorrectas;
        this.respuestasIncorrectas = respuestasIncorrectas;
    }

    public String getResultadoFinal() {
        return resultadoFinal;
    }
    public int getRespuestasCorrectas() {
        return respuestasCorrectas;
    }

    public void setRespuestasCorrectas(int respuestasCorrectas) {
        this.respuestasCorrectas = respuestasCorrectas;
    }

    public int getRespuestasIncorrectas() {
        return respuestasIncorrectas;
    }

    public void setRespuestasIncorrectas(int respuestasIncorrectas) {
        this.respuestasIncorrectas = respuestasIncorrectas;
    }

    public void setResultadoFinal(String resultadoFinal) {
        this.resultadoFinal = resultadoFinal;
    }

    public ArrayList<Resultado> getListaresultados() {
        if(listaresultados==null){
            listaresultados = new ArrayList<>();
        }
        return listaresultados;
    }

    public void setListaresultados(ArrayList<Resultado> listaresultados) {
        this.listaresultados = listaresultados;
    }
}
